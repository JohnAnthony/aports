# Contributor: Matthias Ahouansou <matthias@ahouansou.cz>
maintainer="Matthias Ahouansou <matthias@ahouansou.cz>"
pkgname=cargo-expand
pkgver=1.0.97
pkgrel=0
pkgdesc="Cargo subcommand to show result of macro expansion"
url="https://github.com/dtolnay/cargo-expand"
arch="all"
license="MIT OR Apache-2.0"
makedepends="cargo-auditable"
depends="cargo"
subpackages="$pkgname-doc"
source="
	$pkgname-$pkgver.tar.gz::https://github.com/dtolnay/cargo-expand/archive/refs/tags/$pkgver.tar.gz
"
options="net"

prepare() {
	default_prepare
	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --release --frozen
}

check() {
	cargo test --frozen
}

package() {
	install -Dm 755 target/release/cargo-expand "$pkgdir"/usr/bin/cargo-expand

	for l in APACHE MIT
	do
		install -Dm 644 LICENSE-"$l" "$pkgdir"/usr/share/licenses/"$pkgname"/LICENSE-"$l"
	done
}

sha512sums="
f5a1eaf837f012cf65b977fadfaab726d9ce62b24764df9b170d6cc316ff3fede2b3a19656fe7456f027aa8db5bc2118ddffe024f6f3848cc9a25f7aecf2571b  cargo-expand-1.0.97.tar.gz
"
